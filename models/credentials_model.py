from __future__ import annotations

from typing import List, Literal, Optional, Union

from pydantic import BaseModel


class CredentialRequiredServices(BaseModel):
    url: str
    user: Optional[str] = None
    user_env: Optional[str] = None
    pass_env: str


class CredentialsModel(BaseModel):
    schemaVersion: str
    kind: Union[Literal["Credentials"], Literal["credentials"]]
    credential_required_services: Optional[List[CredentialRequiredServices]] = [
    ]
