from __future__ import annotations

from typing import Dict, List, Literal, Optional, Union

from pydantic import BaseModel, constr


class repoInfo(BaseModel):
    url: str
    description: Optional[str] = None


class repositories(BaseModel):
    __root__: Union[Dict[constr(
        regex=r'^[a-zA-Z0-9-_]+$'), List[repoInfo]], List]


class includes(BaseModel):
    url: Optional[str] = None
    local: Optional[str] = None


class sbom(BaseModel):
    url: Optional[str] = None
    local: Optional[str] = None
    oci: Optional[str] = None


class metadata(BaseModel):
    name: str
    version: Union[str, int]
    description: str


class ManifestModel(BaseModel):
    schemaVersion: str
    kind: Union[Literal["Manifest"], Literal["manifest"]]
    metadata: metadata
    sbom: Optional[List[sbom]] = []
    includes: Optional[List[includes]] = []
    repositories: Optional[repositories] = []
