from __future__ import annotations

from typing import Any, Dict, List, Literal, Optional, Union

from pydantic import BaseModel, Extra, constr


class Stage(BaseModel):
    class Config:
        extra = Extra.forbid

    name: str
    config: Optional[Dict[str, Any]] = None


class Plugins(BaseModel):
    component_coverage: Optional[Union[
        Literal["OPTIONAL"], Literal["EXACTLY_ONCE"],
        Literal["AT_LEAST_ONCE"], Literal["NO_MORE_THAN_ONCE"]
    ]] = None
    plugins: List[Stage]


class Stages(BaseModel):
    __root__: Dict[constr(regex=r'^[a-zA-Z0-9-_]+$'), Plugins]


class TransferModel(BaseModel):
    class Config:
        extra = Extra.forbid

    schemaVersion: str
    kind: Union[Literal["Transfer"], Literal["transfer"]]
    stages: Stages
    max_processes: Optional[int] = 3
